/* This is the first part of MAD Assignment */

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>

#include <stdio.h> //header file for standard input output
#include <stdlib.h> //contains malloc function
#include <string.h> //including string header

#include "customer.h"
#include "config.h" // auto generated

void load_menu(void);
void mainfunc(void);
void rest(void);

int main()
{
  load_menu();
  return 0;
}

void load_menu(void)
{
  int choice;
 do
    {
        printf("\nMenu\n\n");
        printf("1. Send data through the server\n");
        printf("2. Rest\n");
        printf("3. Exit\n");
        scanf("%d",&choice);
 
        switch(choice)
        {
            case 1: mainfunc();
                break;
            case 2: rest();
                break;
            case 3: printf("Quitting program!\n");
                exit(0);
                break;
            default: printf("Invalid choice!\n");
                break;
        }
 
    } while (choice != 3);
 
}
 
void mainfunc(void)
{
   Customer *cust1;
  int sock;
  struct sockaddr_in servaddr;
  char buffer[MAX_BUFFER];
  ssize_t bytes_encoded, bytes_sent;

if ( (sock = socket(AF_INET, SOCK_STREAM, 0)) < 0 ) {
    fprintf(stderr, "Error creating socket.\n");
    exit(EXIT_FAILURE);
  }
  
  memset(&servaddr, 0, sizeof(servaddr));
  servaddr.sin_family      = AF_INET;
  servaddr.sin_addr.s_addr = inet_addr(HOST_IP);
  servaddr.sin_port        = htons(HOST_PORT);

  if (connect(sock, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0) {
    fprintf(stderr, "Error calling connect()\n");
    exit(EXIT_FAILURE);
  }
 

  cust1 = make_customer(1234);
  make_customer_active(cust1);
  set_customer_name(cust1, "Nikolay");

  bytes_encoded = serialize_customer (buffer, cust1);
  printf("bytes_encoded = %d\n", bytes_encoded);

  bytes_sent = send(sock, buffer, bytes_encoded, 0);

 if (bytes_sent != bytes_encoded) {
    fprintf(stderr, "Error calling send()\n");
    exit(EXIT_FAILURE);
  }

 if ( close(sock) < 0 ) {
    fprintf(stderr, "Error calling close()\n");
    exit(EXIT_FAILURE);
  }

  free_customer(cust1);

    return;
}      
 
void rest(void)
{
  int n;
  printf("\nEnter customer age\n");
  scanf("%d", &n);

  while(n <18)
    {
      fprintf(stderr, "\nThe customer can not be under 18()\n");
    exit(EXIT_FAILURE);
	}
  
    printf("\nCustomer is over 18\n");

    return 0;
}


/* End of main */
