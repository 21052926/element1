
typedef struct {
  unsigned id;
  int active;
  char *name;
} Customer;

Customer *make_customer(unsigned id);
void free_customer(Customer *cust);
void make_customer_active(Customer *cust);
void set_customer_name(Customer *cust, char *name);
int is_customer_active(Customer *cust);
int serialize_customer(char *buffer, Customer *cust);
int deserialize_customer(char *buffer, Customer *cust);
void print_customer(Customer *cust);
Customer *alloc_blank_customer();

