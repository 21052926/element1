/* This is student.c file which as a part of MAD assignment 1 is referenced and used in main.c */

#include <stdio.h>
#include <stdlib.h>

#include <string.h>

#include "config.h" // auto generated
#include "customer.h"


Customer *make_customer(unsigned id)
{
  
  Customer *cust;
  
  if ((cust = (Customer *)malloc(sizeof(Customer))) == NULL) {
    fprintf(stderr, "Failed to allocate Customer structure!\n");
    exit(EXIT_FAILURE);
  }
  cust->active = 0;
  cust->id = id;
  cust->name = NULL;
  
  return cust;
}

void free_customer(Customer *cust)
{
  free(cust->name);
  free(cust);
}

void make_customer_active(Customer *cust)
{
  cust->active = 1;
}

void set_customer_name(Customer *cust, char *name)
{
  cust->name = strdup(name);
}

int is_customer_active(Customer *cust)
{
  return cust->active;
}

int serialize_customer(char *buffer, Customer *cust)
{
  size_t offset = 0;

  memcpy(buffer, &cust->id, sizeof(cust->id));
  offset = sizeof(cust->id);
  memcpy(buffer+offset, &cust->active, sizeof(cust->active));
  offset = offset + sizeof(cust->active);
  memcpy(buffer+offset, cust->name, strlen(cust->name)+1);
  offset = offset + strlen(cust->name)+1;

  return offset;
}

int deserialize_customer(char *buffer, Customer *cust)
{
  size_t offset = 0;

  memcpy(&cust->id, buffer, sizeof(cust->id));
  offset = sizeof(cust->id);
  memcpy(&cust->active, buffer+offset, sizeof(cust->active));
  offset = offset + sizeof(cust->active);
  memcpy(cust->name, buffer+offset, strlen(buffer+offset)+1);
  offset = offset + strlen(buffer+offset)+1;

  return offset;
}

void print_customer(Customer *cust)
{
  printf("Customer id:%d\n", cust->id);
  printf("Cutomer name:%s\n", cust->name);
}

Customer *alloc_blank_customer() 
{

  Customer *cust;

  if ((cust = (Customer *)malloc(sizeof(Customer))) == NULL) {
    fprintf(stderr, "Failed to allocate Customer structure!\n");
    exit(EXIT_FAILURE);
  }
  cust->active = 0;
  cust->id = 0;
  if ((cust->name = malloc(MAX_NAME)) == NULL) {
    fprintf(stderr, "Failed to allocate name!\n");
    exit(EXIT_FAILURE);
  }

  return cust;
}
